
require("spritebuilder.functions")

local Config = require("spritebuilder.CCBConfig")
local translation = Config:getInstance():getTranslation()

local function stringFromOptions(options)
    local data = options.string or options.title
    assert(data ~= nil, "Error: stringFromOptions data is nil")
    return data[2] and translation:str(data[1]) or data[1]
end

---------------------------
-- CCBLoader
---------------------------

local CCBLoader = class("CCBLoader")

-- fonts path
local FONTS_PATH = "fonts/"

-- points
local POSITION_TYPE_IN_POINTS  = 0
local POSITION_TYPE_IN_UI_POINTS = 1
local POSITION_TYPE_PERCENT = 2

-- size
local SIZE_TYPE_IN_POINTS = 0
local SIZE_TYPE_IN_UI_POINTS = 1
local SIZE_TYPE_PERCENT = 2
local SIZE_TYPE_INSET_IN_POINTS = 3     -- todo, not support
local SIZE_TYPE_INSET_IN_UI_POINTS = 4  -- toto, not support

-------------------------
-- Set Properties
-------------------------

local function setNodeProps(node, options)
    -- contentSize
    local size = options.contentSize or cc.size(0, 0)
    node:setContentSize(size)
    -- posotion
    local x, y = 0, 0
    if options.position then
        x, y = options.position.x, options.position.y
    end
    node:setPosition(x, y)
    -- anchorPoint
    local anchorX, anchorY = 0.5, 0.5
    if options.anchorPoint then
        anchorX, anchorY = options.anchorPoint.x, options.anchorPoint.y
    end
    node:setAnchorPoint(anchorX, anchorY)
    -- scale
    local scaleX, scaleY = 1, 1
    if options.scale then
        scaleX, scaleY = options.scale.x, options.scale.y
    end
    node:setScaleX(scaleX)
    node:setScaleY(scaleY)
    -- rotation
    local rotation = options.rotation or 0
    node:setRotation(rotation)
    -- visible
    local visible = options.visible or true
    node:setVisible(visible)
    -- ignoreAnchor
    local ignoreAnchor = options.ignoreAnchorPointForPosition or false
    node:ignoreAnchorPointForPosition(ignoreAnchor)
end

local function setSpriteProps(spr, options)
    local cache = cc.SpriteFrameCache:getInstance()
    if options.spriteFrame.plist ~= "" then
        local frame = cache:getSpriteFrame(options.spriteFrame.spriteFrameName)
        spr:setSpriteFrame(frame)
    else
        local filename = options.spriteFrame.spriteFrameName
        spr:setTexture(filename)
    end

    if options.opacity then
        spr:setOpacity(options.opacity)
    end

    if options.color then
        spr:setColor(options.color)
    end
    
    local flipX, flipY = false, false
    if options.flip then
        flipX, flipY = options.flip.flipX, options.flip.flipY
    end
    spr:setFlippedX(flipX)
    spr:setFlippedY(flipY)
end

local function setScale9SpriteProps(spr, options)
    local cache = cc.SpriteFrameCache:getInstance()
    local frame = cache:getSpriteFrame(options.spriteFrame.spriteFrameName)
    spr:setSpriteFrame(frame)
    spr:setPreferredSize(options.preferredSize)
    spr:setOpacity(options.opacity)
    spr:setColor(options.color)
end

local function setLabelBMFontProps(label, options)
    local op = options.opacity or 255
    label:setOpacity(op)
    local color = options.color or cc.c3b(255, 255, 255)
    label:setColor(color)
end

local function setParticleProps(particle, options)
    -- 0 Gravity, 1 Radius
    particle:setEmitterMode(options.emitterMode)
    -- posVar
    local posVar = options.posVar
    particle:setPosVar(cc.p(posVar[1], posVar[2]))
    -- emissionRate
    particle:setEmissionRate(options.emissionRate)
    -- duration (-1 forever)
    particle:setDuration(options.duration)
    -- totalParticles
    particle:setTotalParticles(options.totalParticles)
    -- life & lifeVar
    particle:setLife(options.life[1])
    particle:setLifeVar(options.life[2])
    -- startSize & startSizeVar
    particle:setStartSize(options.startSize[1])
    particle:setStartSizeVar(options.startSize[2])
    -- endSize & endSizeVar
    particle:setEndSize(options.endSize[1])
    particle:setEndSizeVar(options.endSize[2])
    -- startSpin & startSpinVar
    particle:setStartSpin(options.startSpin[1])
    particle:setStartSpinVar(options.startSpin[2])
    -- endSpin & endSpinVar
    particle:setEndSpin(options.endSpin[1])
    particle:setEndSpinVar(options.endSpin[2])
    -- angle & angleVar
    particle:setAngle(options.angle[1])
    particle:setAngleVar(options.angle[2])
    -- startColor & startColorVar
    particle:setStartColor(options.startColor[1])
    particle:setStartColorVar(options.startColor[2])
    -- endColor & endColorVar
    particle:setEndColor(options.endColor[1])
    particle:setEndColorVar(options.endColor[2])
    -- blendFunc
    local blendFuncVal = options.blendFunc
    particle:setBlendFunc(cc.blendFunc(blendFuncVal.src, blendFuncVal.dst))
    -- gravity
    particle:setGravity(cc.p(options.gravity[1], options.gravity[2]))
    -- speed & speedVar
    particle:setSpeed(options.speed[1])
    particle:setSpeedVar(options.speed[2])
    -- tangentialAccel & tangentialAccelVar
    particle:setTangentialAccel(options.tangentialAccel[1])
    particle:setTangentialAccelVar(options.tangentialAccel[2])
    -- radialAccel & radialAccelVar
    particle:setRadialAccel(options.radialAccel[1])
    particle:setRadialAccelVar(options.radialAccel[2])
    -- texture
    local tex = display.loadImage(options.texture)
    particle:setTexture(tex)

    -- TODO, not impl resetOnVisibilityToggle (value, true/false)
end

-------------------------
-- Create Functions
-------------------------

local function layerCreateFunc(options)
    local layer = cc.Layer:create()
    setNodeProps(layer, options)
    return layer
end

local function nodeCreateFunc(options)
    local node = cc.Node:create()
    setNodeProps(node, options)
    return node
end

local function spriteCreateFunc(options)
    local spr = cc.Sprite:create()
    setNodeProps(spr, options)
    setSpriteProps(spr, options)
    return spr
end

local function scale9SpriteCreateFunc(options)
    local spClass = ccui.Scale9Sprite or cc.Scale9Sprite
    local spr = spClass:create()
    setNodeProps(spr, options)
    setScale9SpriteProps(spr, options)
    return spr
end

local function controllButtonCreateFunc(options)
    -- dump(options, "control btn's options")

    local text = stringFromOptions(options)
    local fontPath = FONTS_PATH .. options.fontName
    local fSize = options.fontSize or 21
    local title
    if cc.FileUtils:getInstance():isFileExist(fontPath) then
        title = cc.Label:createWithTTF(text, fontPath, fSize)
    else
        title = cc.Label:createWithSystemFont(text, options.fontName, fSize)
    end
    local titleAnchorX, titleAnchorY = 0.5, 0.5
    if options.labelAnchorPoint then
        titleAnchorX, titleAnchorY = options.labelAnchorPoint.x, options.labelAnchorPoint.y
    end
    title:setAnchorPoint(cc.p(titleAnchorX, titleAnchorY))

    local norFile = options["backgroundSpriteFrame|Normal"].spriteFrameName
    local highFile = options["backgroundSpriteFrame|Highlighted"].spriteFrameName
    local disFile = options["backgroundSpriteFrame|Disabled"].spriteFrameName
    local selFile = options["backgroundSpriteFrame|Selected"].spriteFrameName
    local zoomWhenHighlighted = options.zoomWhenHighlighted
    local btnsize = options.preferredSize
    local spClass = ccui.Scale9Sprite or cc.Scale9Sprite
    local norSprite = spClass:createWithSpriteFrameName(norFile)
    norSprite:setPreferredSize(btnsize)
    local ret =  cc.ControlButton:create(title, norSprite)
    ret:setZoomOnTouchDown(zoomWhenHighlighted)
    -- 文件名中包含nil.png表示不创建这个图片，节约资源和内存
    if highFile and string.len(highFile) > 0 then
        local highSprite = spClass:createWithSpriteFrameName(highFile)
        highSprite:setPreferredSize(btnsize)
        ret:setBackgroundSpriteForState(highSprite, 
                                        cc.CONTROL_STATE_HIGH_LIGHTED);
    end
    if disFile and string.len(disFile) > 0 then
        local disSprite = spClass:createWithSpriteFrameName(disFile)
        disSprite:setPreferredSize(btnsize)
        ret:setBackgroundSpriteForState(disSprite, 
                                        cc.CONTROL_STATE_DISABLED);
    end

    local status = {"Normal", "Highlighted", "Disabled", "Selected"}
    local states = {}
    for i = 1, #status do 
        local key =  "labelColor|" .. status[i]
        local labelColor = options[key]
        if labelColor then
            ret:setTitleColorForState(labelColor, cc.CONTROL_STATE_NORMAL+(i-1))
        end
    end
    ret:setPreferredSize(btnsize)
    setNodeProps(ret, options)
    return ret
end

local function labelTTFCreateFunc(options)
    -- dump(options, "labelTTFCreateFunc")

    local text = stringFromOptions(options)
    local fontPath = FONTS_PATH .. options.fontName
    local font = options.fontName
    local size = options.fontSize
    local dimensions = options.dimensions
    local align = options.horizontalAlignment
    local valign = options.verticalAlignment
    
    local label
    if cc.FileUtils:getInstance():isFileExist(font) then
        label = cc.Label:createWithTTF(text, fontPath, size, dimensions, align, valign)
    else
        label = cc.Label:createWithSystemFont(text, font, size, dimensions, align, valign)
    end
    local outlineColor = cc.convertColor(options.outlineColor, "4b")
    label:enableOutline(outlineColor, options.outlineWidth)
    label:enableShadow(cc.convertColor(options.shadowColor, "4b"), options.shadowOffset, options.shadowBlurRadius)
    label:setColor(cc.convertColor(options.fontColor, "4b"))
    setNodeProps(label, options)
    return label
end

local function labelBMFontCreateFunc(options)
    -- options.string[2] and translation:str(options.string[1]) or options.string[1]
    local text = stringFromOptions(options)
    local fontPath = FONTS_PATH .. options.fntFile
    local label = cc.Label:createWithBMFont(fontPath, text)
    setNodeProps(label, options)
    setLabelBMFontProps(label, options)
    return label
end

local function ListViewCreateFunc(options)
    print("ListViewCreateFunc")
    local spriteFrameName = "#" .. options.spriteFrame.spriteFrameName
    local w = options.preferredSize.width
    local h = options.preferredSize.height
    local x = options.position.x
    local y = options.position.y
    local ax = options.anchorPoint.x
    local ay = options.anchorPoint.y
    local color = options.color or cc.c3b(255, 255, 255)
    local dir = options.isHorizontal and 
                cc.ui.UIScrollView.DIRECTION_HORIZONTAL or cc.ui.UIScrollView.DIRECTION_VERTICAL
    local listView = cc.ui.UIListView.new {
        bg = spriteFrameName,
        bgScale9 = true,
        viewRect = cc.rect(x, y, w, h),
        direction = dir}
    listView:setAnchorPoint(cc.p(ax, ay))
    listView:setContentSize(w,h)
    return listView
end

local function PageViewCreateFunc(options)
    local w = options.preferredSize.width
    local h = options.preferredSize.height
    local x = options.position.x
    local y = options.position.y
    local pageView = cc.ui.UIPageView.new {
        viewRect = cc.rect(x,y,w,h),
        column = options.column,
        row = options.row,
        columnSpace = options.columnSpace,
        rowSpace = options.rowSpace,
        padding = {
            left = options.paddingLeft, 
            right = options.paddingRight, 
            top = options.paddingTop, 
            bottom = options.paddingBottom
        },
        circle = options.isCircle
    }
    return pageView
end

local function nodeColorCreateFunc(options)
    local color = options.color
    local layer = cc.LayerColor:create(cc.c3b(color.r, color.g, color.b, 1))
    layer:setOpacity(options.opacity)
    setNodeProps(layer, options)
    return layer
end

local function particleCreateFunc(options)
    local particle = cc.ParticleSystemQuad:create()
    setNodeProps(particle, options)
    setParticleProps(particle, options)
    return particle
end

local baseClassCreateFuncs = {
    CCLayer = layerCreateFunc,
    CCNode = nodeCreateFunc,
    CCSprite = spriteCreateFunc,
    CCScale9Sprite = scale9SpriteCreateFunc,
    CCButton = controllButtonCreateFunc,
    CCLabelTTF = labelTTFCreateFunc,
    CCLabelBMFont  = labelBMFontCreateFunc,
    CCListView = ListViewCreateFunc,
    CCPageView = PageViewCreateFunc,
    CCNodeColor = nodeColorCreateFunc,
    CCParticleSystem = particleCreateFunc,
}

-------------------------
-- Parse Functions
-------------------------

-- 临时变量，用来保存posX与posY的类型
local posTypeX = POSITION_TYPE_IN_POINTS
local posTypeY = POSITION_TYPE_IN_POINTS

-- 该方法只给position独享，因为动画位置的关系
local function positionParseFunc(baseVal, parent, data)
    local x, y = 0, 0
    posTypeX = data[4]
    if parent and data[4] == POSITION_TYPE_PERCENT then 
        x = data[1] * parent:getContentSize().width
    elseif (data[4] == POSITION_TYPE_IN_POINTS or data[4] == POSITION_TYPE_IN_UI_POINTS) then
        x = data[1]
    end
    posTypeY = data[5]
    if parent and data[5] == POSITION_TYPE_PERCENT then 
        y = data[2] * parent:getContentSize().height
    elseif (data[5] == POSITION_TYPE_IN_POINTS or data[5] == POSITION_TYPE_IN_UI_POINTS) then
        y = data[2]
    end
    return {x = x, y = y}
end

local function position2ParseFunc(baseVal, parent, data)
    local x, y = 0, 0
    if parent and data[4] == POSITION_TYPE_PERCENT then 
        x = data[1] * parent:getContentSize().width
    elseif (data[4] == POSITION_TYPE_IN_POINTS or data[4] == POSITION_TYPE_IN_UI_POINTS) then
        x = data[1]
    end
    if parent and data[5] == POSITION_TYPE_PERCENT then 
        y = data[2] * parent:getContentSize().height
    elseif (data[5] == POSITION_TYPE_IN_POINTS or data[5] == POSITION_TYPE_IN_UI_POINTS) then
        y = data[2]
    end
    return {x = x, y = y}
end

local function contentSizeParseFunc(data, parent)
    local w, h = data[1], data[2]
    if data[3] == SIZE_TYPE_PERCENT then
        local pw = parent and parent:getContentSize().width or display.width
        w = pw * data[1]
    end
    if data[4] == SIZE_TYPE_PERCENT then
        local ph = parent and parent:getContentSize().height or display.height
        h = ph * data[2]
    end
    return {width = w, height = h}
end

local function anchorPointParseFunc(data)
    return {x = data[1], y = data[2]}
end

local function scaleParseFunc(data)
    return {x = data[1], y = data[2]} 
end

local function displayFrameParseFunc(data)
    return {plist = data[1], spriteFrameName = data[2]}
end

local function colorParseFunc(data)
    return cc.c3b(data[1]*255, data[2]*255, data[3]*255)
end

local function opacityParseFunc(data)
    return data * 255
end

local function flipParseFunc(data)
    return {flipX = data[1], flipY = data[2]}
end

local function BlendFuncParseFunc(data)
    return {src = data[1], dst = data[2]}
end

local function spriteFrameParseFunc(data)
    return {plist = data[1], spriteFrameName = data[2]}
end

local function preferredSizeParseFunc(data)
    return {width = data[1], height = data[2]}
end

local function ccControlParseFunc(data)
    return {callback_func_name = data[1] }
end

local function labelAnchorPointParseFunc(data)
    return {x = data[1], y = data[2]}
end

local function labelTTFFontSizeParseFunc(data)
    return data[1]
end

local function demensionsParseFunc(data)
    return {width = data[1], height = data[2]}
end

local function horizontalPaddingFunc(data)
    return data[1]
end

local function verticalPaddingFunc(data)
    return data[1]
end

local function floatScaleParseFunc(data)
    return data[1]
end

local function emitterModeParseFunc(data)
    return data
end

local function emissionRateParseFunc(data)
    return data
end

local function durationParseFunc(data)
    return data
end

local function endSizeParseFunc(data)
    return {data[1], data[2]}
end

local function startSpinParseFunc(data)
    return {data[1], data[2]}
end

local function endSpinParseFunc(data)
    return {data[1], data[2]}
end

local function angleParseFunc(data)
    return {data[1], data[2]}
end

local function startColorParseFunc(data)
    local c1 = data[1]
    local r1 = cc.c4f(c1[1], c1[2], c1[3], c1[4])
    local c2 = data[2]
    local r2 = cc.c4f(c2[1], c2[2], c2[3], c2[4])
    return {r1, r2}
end

local function endColorParseFunc(data)
    local c1 = data[1]
    local r1 = cc.c4f(c1[1], c1[2], c1[3], c1[4])
    local c2 = data[2]
    local r2 = cc.c4f(c2[1], c2[2], c2[3], c2[4])
    return {r1, r2}
end

local function gravityParseFunc(data)
    return {data[1], data[2]}
end

local function speedParseFunc(data)
    return {data[1], data[2]}
end

local function tangentialAccelParseFunc(data)
    return {data[1], data[2]}
end

local function radialAccelParseFunc(data)
    return {data[1], data[2]}
end

local function textureParseFunc(data)
    return data
end

local function posVarParseFunc(data)
    return {data[1], data[2]}
end

local function totalParticlesParseFunc(data)
    return data
end

local function lifeParseFunc(data)
    return {data[1], data[2]}
end

local function startSizeParseFunc(data)
    return {data[1], data[2]}
end

local propParseFuncs = {
    visible = "self",
    position = positionParseFunc,
    rotation = "self",
    contentSize = contentSizeParseFunc,
    anchorPoint = anchorPointParseFunc,
    scale = scaleParseFunc,
    ignoreAnchorPointForPosition = "self",  -- “self” 相当于 local function parseFunc(data) return data end
    displayFrame = displayFrameParseFunc,
    color = colorParseFunc,
    opacity = opacityParseFunc,
    flip = flipParseFunc,
    blendFunc = BlendFuncParseFunc,
    spriteFrame = spriteFrameParseFunc,
    preferredSize = preferredSizeParseFunc,
    insetLeft = "self",
    insetTop = "self",
    insetRight = "self",
    insetBottom = "self",
    fntFile = "self",
    string = "self",
    ccControl = ccControlParseFunc,
    enabled = "self",
    labelAnchorPoint = labelAnchorPointParseFunc,
    zoomWhenHighlighted = "self",
    isHorizontal = "self",
    column = "self",
    row = "self",
    columnSpace = "self",
    rowSpace = "self",
    paddingLeft = "self",
    paddingRight = "self",
    paddingTop = "self",
    paddingBottom = "self",
    isCircle = "self";
    fontName = "self",
    fontSize = labelTTFFontSizeParseFunc,
    horizontalAlignment = "self",
    verticalAlignment = "self",
    dimensions = demensionsParseFunc,
    horizontalPadding = horizontalPaddingFunc,
    verticalPadding = verticalPaddingFunc,
    title = "self",
    fontColor = colorParseFunc,
    outlineColor = colorParseFunc,
    outlineWidth = floatScaleParseFunc,
    shadowColor = colorParseFunc,
    shadowBlurRadius = floatScaleParseFunc,
    shadowOffset = position2ParseFunc,
    emitterMode = emitterModeParseFunc,
    emissionRate = emissionRateParseFunc,
    duration = durationParseFunc,
    endSize = endSizeParseFunc,
    startSpin = startSpinParseFunc,
    endSpin = endSpinParseFunc,
    angle = angleParseFunc,
    startColor = startColorParseFunc,
    endColor = endColorParseFunc,
    gravity = gravityParseFunc,
    speed = speedParseFunc,
    tangentialAccel = tangentialAccelParseFunc,
    radialAccel = radialAccelParseFunc,
    texture = textureParseFunc,
    posVar = posVarParseFunc,
    totalParticles = totalParticlesParseFunc,
    life = lifeParseFunc,
    startSize = startSizeParseFunc,
}

propParseFuncs["backgroundSpriteFrame|Normal"] = spriteFrameParseFunc
propParseFuncs["backgroundSpriteFrame|Highlighted"] = spriteFrameParseFunc
propParseFuncs["backgroundSpriteFrame|Disabled"] = spriteFrameParseFunc
propParseFuncs["backgroundSpriteFrame|Selected"] = spriteFrameParseFunc

propParseFuncs["labelColor|Normal"] = colorParseFunc
propParseFuncs["labelColor|Highlighted"] = colorParseFunc
propParseFuncs["labelColor|Disabled"] = colorParseFunc
propParseFuncs["labelColor|Selected"] = colorParseFunc

propParseFuncs["labelOpacity|Normal"] = "self"
propParseFuncs["labelOpacity|Highlighted"] = "self"
propParseFuncs["labelOpacity|Disabled"] = "self"
propParseFuncs["labelOpacity|Selected"] = "self"

propParseFuncs["backgroundColor|Normal"] = colorParseFunc
propParseFuncs["backgroundColor|Highlighted"] = colorParseFunc
propParseFuncs["backgroundColor|Disabled"] = colorParseFunc
propParseFuncs["backgroundColor|Selected"] = colorParseFunc

local function parseProps(props, parent)
    local ret = {}
    for i = 1, #props do
        local p = props[i]
        local name = p.name
        local func = propParseFuncs[name]
        local v = p["baseValue"] or p["value"]
        if func and type(func) == 'function' then
            ret[name] = func(v, parent, p["value"]) -- 第3个参数给position使用，其不能使用baseValue(posType无法确定)
        elseif func and type(func) == "string" and func == "self" then
            ret[name] = v
        end
    end
    return ret
end

---------------
-- 重构 Parse
---------------

local function parseFunc_Color3(data)
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then return { baseVal[1] * 255, baseVal[2] * 255, baseVal[3] * 255, baseVal[4] * 255 } end
    -- value
    local val = data["value"]
    return { val[1] * 255, val[2] * 255, val[3] * 255, val[4] * 255 }
end

local function parseFunc_Color4(data)
    return parseFunc_Color3(data)
end

-- todo, 考虑是否把 POSITION_TYPE 加进来
local function parseFunc_Position(data, parent)
    -- value
    local val = data["value"]
    local x, y = 0, 0
    if parent and val[4] == POSITION_TYPE_PERCENT then 
        x = val[1] * parent:getContentSize().width
    elseif (val[4] == POSITION_TYPE_IN_POINTS or val[4] == POSITION_TYPE_IN_UI_POINTS) then
        x = val[1]
    end
    if parent and val[5] == POSITION_TYPE_PERCENT then 
        y = val[2] * parent:getContentSize().height
    elseif (val[5] == POSITION_TYPE_IN_POINTS or val[5] == POSITION_TYPE_IN_UI_POINTS) then
        y = val[2]
    end
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then
        if parent and val[4] == POSITION_TYPE_PERCENT then
            x = baseVal[1] * parent:getContentSize().width
        elseif (val[4] == POSITION_TYPE_IN_POINTS or val[4] == POSITION_TYPE_IN_UI_POINTS) then
            x = baseVal[1]
        end
        if parent and val[5] == POSITION_TYPE_PERCENT then 
            y = baseVal[2] * parent:getContentSize().height
        elseif (val[5] == POSITION_TYPE_IN_POINTS or val[5] == POSITION_TYPE_IN_UI_POINTS) then
            y = baseVal[2]
        end
    end
    return { x = x, y = y }
end

local function parseFunc_Point(data)
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then return cc.p(baseVal[1], baseVal[2]) end
    -- value
    local val = data["value"]
    return cc.p(val[1], val[2])
end

local function parseFunc_Scale(data)
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then return { x = baseVal[1], y = baseVal[2] } end
    -- value
    local val = data["value"]
    return { x = val[1], y = val[2] }
end

local function parseFunc_Degrees(data)
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then return baseVal end
    -- value
    return data["value"] 
end

local function parseFunc_FloatXY(data)
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then return { x = baseVal[1], y = baseVal[2] } end
    -- value
    local val = data["value"]
    return { x = val[1], y = val[2] }
end

local function parseFunc_Size(data, parent)
    -- value
    local val = data["value"]
    local w, h = val[1], val[2]
    if val[3] == SIZE_TYPE_PERCENT then
        local pw = parent and parent:getContentSize().width or display.width
        w = pw * val[1]
    end
    if val[4] == SIZE_TYPE_PERCENT then
        local ph = parent and parent:getContentSize().height or display.height
        h = ph * val[2]
    end
    -- baseValue
    local baseVal = data["baseValue"]
    if baseVal then
        if val[3] == SIZE_TYPE_PERCENT then
            local pw = parent and parent:getContentSize().width or display.width
            w = pw * baseVal[1]
        end
        if val[4] == SIZE_TYPE_PERCENT then
            local ph = parent and parent:getContentSize().height or display.height
            h = ph * baseVal[2]
        end
    end
    return { width = w, height = h }
end

-------------------------
-- Sequence Functions
-------------------------

local function parseSequenceProps(allseqs, callbacks)
    local ret = {}
    local CCSeq = require("spritebuilder.CCBSeq")
    for i = 1, #allseqs do
        local s = CCSeq.new(allseqs[i], callbacks)
        ret[s.name] = s
    end
    return ret
end

local function getSeqWithId(seq, id)
    for k,v in pairs(seq) do
        if tonumber(v.sequenceId) == tonumber(id) then
            return v
        end
    end
    assert(false, string.format("not fond seq id = %d", id))
end

local function setNodeBaseValue(node, baseClassName, options)
    node.baseClassName = baseClassName
    node.baseValue = options
end

-------------------------
-- Helper Functions
-------------------------

local function createNodeWithBaseClassName(rootdata, parent, childrenList, seq)
    local baseClassName = rootdata["baseClass"]
    print("baseClassName = ", baseClassName)
    local props = rootdata["properties"]
    local options = parseProps(props, parent)

    local node = baseClassCreateFuncs[baseClassName](options)
    setNodeBaseValue(node, baseClassName, options)
    
    -- fix, for position type fix
    node.posTypeX_ = posTypeX
    node.posTypeY_ = posTypeY
    node.parent_ = parent
    
    -- fix, animated properties
    node.animatedProperties_ = {}

    -- used for create actions
    local animatedProperties = rootdata["animatedProperties"]
    if animatedProperties then
        print("-----animatedProperties----")
        print("node has ", table.nums(animatedProperties), " timeline!, in sequense ")
        for id, animatedProperty in pairs(animatedProperties) do
            local s = getSeqWithId(seq, id)
            s:addAnimForNode(node, animatedProperty, s.name)
        end
    end
    local children = rootdata["children"]
    if #children > 0 then
        print(rootdata.baseClass, "has", #children, "children")
        for i = 1, #children do
            local child = children[i]
            local nextList = childrenList
            -- 如果该节点还有子节点，则再创建一个table 用来保存子节点的数据，递归到根节点
            -- create new table to save to reference of the children
            if #child.children > 0 then
                childrenList[child.memberVarAssignmentName] = {}
                nextList = childrenList[child.memberVarAssignmentName]
            end
            -- 递归创建子节点
            -- create the children recursively
            local c = createNodeWithBaseClassName(child, node, nextList, seq)
            if c then
                node:addChild(c)
            else
                printf("!!!!!! fail to parse node name = %s, type = %s,memberVarAssignmentName = %s", 
                    child.displayName, child.baseClass, child.memberVarAssignmentName)
                assert(false)
            end
            -- 这里遍历到叶节点了，进行赋值
            -- to the 'leaf nodes'
            if child.memberVarAssignmentName
                and child.memberVarAssignmentName ~= "" 
                and #child.children == 0 then
                childrenList[child.memberVarAssignmentName] = c
            end
        end
    end
    return node
end

function CCBLoader.loadCCB(jsonFileName, callbacks, root)
    print("load CCB - " .. jsonFileName)
    
    local filedata = cc.HelperFunc:getFileData(jsonFileName)
    local datas = json.decode(filedata)
    local rootdatas = datas["nodeGraph"]
    local seqdatas = datas["sequences"]
    local childrenList = {}
    local allseqs = parseSequenceProps(seqdatas, callbacks)
    local ccbNode = createNodeWithBaseClassName(rootdatas, root, childrenList, allseqs)
    if root then root:addChild(ccbNode) end

    return ccbNode, childrenList, allseqs
end

-- 预加载音效文件(未支持pitch, pan, gain的功能)
function CCBLoader.preloadSounds(jsonFileName, callback)
    print("preload CCB - " .. jsonFileName)

    local filedata = cc.HelperFunc:getFileData(jsonFileName)
    local datas = json.decode(filedata)
    local seqdatas = datas["sequences"]
    -- sound infos
    local sounds = {}
    for _, seq in ipairs(seqdatas) do
        local kframes = seq["soundChannel"]["keyframes"]
        for _, frame in ipairs(kframes) do
            local soundfile = frame.value[1]
            audio.preloadSound(soundfile)
        end
    end

    -- callback
    if callback then callback() end
end

-- 卸载音效文件
function CCBLoader.unloadSounds(jsonFileName)
    print("unload CCB - " .. jsonFileName)

    local filedata = cc.HelperFunc:getFileData(jsonFileName)
    local datas = json.decode(filedata)
    local seqdatas = datas["sequences"]
    -- sound infos
    local sounds = {}
    for _, seq in ipairs(seqdatas) do
        local frames = seq["soundChannel"]["frames"]
        for _, frame in ipairs(frames) do
            local soundfile = frame.value[1]
            audio.unloadSound(soundfile)
        end
    end
end

function CCBLoader.playSeq(node, allseq, seq)
    -- check
    if not seq then
        printf("not found anim seq [%s]", seq)
        return
    end
    -- play callfunc & sounds
    local callfuncs, sounds = seq:getSeqData()
    for i = 1, #callfuncs do node:runAction(callfuncs[i]) end
    for i = 1, #sounds do node:runAction(sounds[i]) end

    -- play recursively
    local function playNextSeq(runningNode, allseq, seq)
        local nextid = seq.chainedSequenceId
        if tonumber(nextid) == -1 then
            return
        end
        local nextSeq = getSeqWithId(allseq, nextid)
        CCBLoader.playSeq(runningNode, allseq, nextSeq)
    end
    local next = delayCall(seq.length, playNextSeq, node, allseq, seq) 
    node:runAction(next)
end

function CCBLoader.playTimeline(node, allseq, name)
    local nm = name or "Default Timeline"
    CCBLoader.playSeq(node, allseq, allseq[nm])
end

return CCBLoader
