
local CCBString = require("spritebuilder.CCBString")

local CCBConfig = {}

CCBConfig.TRANS_FILE = "Strings.json"

function CCBConfig:new()
    local instance = nil 
    return function()
        if instance then return instance end 
        local o = {}
        setmetatable(o, self)  
        self.__index = self  
        instance = o

        self:init()
        return o 
    end 
end

CCBConfig.getInstance = CCBConfig:new();

function CCBConfig:init()
    -- init the translation
    local path = CCBConfig.TRANS_FILE
    -- local language = cc.Application:getInstance():getCurrentLanguage()
    self.translation = CCBString.new(path, 0) --language
end

function CCBConfig:getTranslation()
    return self.translation
end

return CCBConfig
