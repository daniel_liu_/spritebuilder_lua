
require("spritebuilder.functions")

--  the data format is:
--[[
{
    "autoPlay": true,
    "length": 2,  --这个sequence的总时间
    "offset": 0,
    "position": 2,
    "resolution": 30,
    "scale": 128,
    "sequenceId": 0,
    "soundChannel": {
    },
    "chainedSequenceId": 0,
    "name": "timeline_default",
    "callbackChannel": {}
}
]]

---------------------------
-- CCBSeq
---------------------------

local CCBSeq = class("CCBSeq")

function CCBSeq:ctor(jsondata, callbacks)
    self.callbacks = callbacks
    self.animatedNodes = {} 
    deepcopy(self, jsondata)
end

local function createEaseWithType(easetype, rate, action)
    -- 0为Instant，所以这里理论上是很难的，基于其它方法里面的duration
    if easetype == 0 or easetype == 1 then return action end
    if easetype == 2 then return cc.EaseIn:create(action, rate) end
    if easetype == 3 then return cc.EaseOut:create(action, rate) end
    if easetype == 4 then return cc.EaseInOut:create(action, rate) end
    if easetype == 5 then return cc.EaseElasticIn:create(action, rate) end  -- period not rate, no check
    if easetype == 6 then return cc.EaseElasticOut:create(action, rate) end -- period not rate, no check
    if easetype == 7 then return cc.EaseElasticInOut:create(action, rate) end -- period not rate, no check
    if easetype == 8 then return cc.EaseBounceIn:create(action) end
    if easetype == 9 then return cc.EaseBounceOut:create(action) end
    if easetype == 10 then return cc.EaseBounceInOut:create(action) end
    if easetype == 11 then return cc.EaseBackIn:create(action) end
    if easetype == 12 then return cc.EaseBackOut:create(action) end
    if easetype == 13 then return cc.EaseBackInOut:create(action) end
    
    assert(false, "unsupported ease type" .. easetype)
end

local function keyFrame_sound_createFunc(data)
    local ret = {}
    local keyframes = data.keyframes
    for i = 1, #keyframes do
        local keyFrame = keyframes[i]
        local delay = keyFrame.time
        local action = delayCall(delay, function()
                audio.playSound(keyFrame.value[1], false)
            end)
        ret[#ret+1] = action
    end
    return ret
end

-- callback中与设定root和document无关
-- 因为lua实在是太灵活了，没必须要这里做得固化
local function keyFrame_callback_createFunc(timeline, data, callbacks)
    local ret = {}
    local keyframes = data.keyframes
    for i = 1, #keyframes do
        local keyFrame = keyframes[i]
        -- sb中配置了callbacks，但是代码中未有实现的检查1
        if nil == callbacks then
            print("Warning .. not set callbacks func for timeline: " .. timeline)
        else
            local func = callbacks[keyFrame.value[1]]
            if not func then
                printf("Warning .. not set callback func for timeline: %s - [%s]", timeline, keyFrame.value[1])
            end
            local action = delayCall(keyFrame.time, func)
            ret[#ret+1] = action
        end
    end
    return ret
end

local function keyFrameCreate(jsondata, createFunc, ...)
    local keyframes = jsondata.keyframes
    local actions = {}
    if keyframes[1].time > 0 then
        actions[#actions+1] = cc.DelayTime:create(keyframes[1].time)
    end

    for i = 1, #keyframes-1 do
        local f1 = keyframes[i]
        local f2 = keyframes[i+1]
        local action = createFunc(f1, f2, i)
        local ease = createEaseWithType(f1.easing.type, f1.easing.opt, action)
        actions[#actions+1] = ease
    end
    return cc.Sequence:create(actions)
end

-- visible to be done
-- visible 因为没有 “没有动画” 这个动画的概念，所有这里特殊处理了动画的开头和结尾
local function keyFrame_visible_action_create(jsondata)
    local function visible(f1, f2, index)
        local show = (index % 2 == 1)
        local duration = f2.time - f1.time
        local act = show and cc.Show:create() or cc.Hide:create()
        return cc.Sequence:create(act, cc.DelayTime:create(duration))
    end
    return keyFrameCreate(jsondata, visible)
end

local function keyFrame_color_action_create(jsondata)
    local function tint(f1, f2)
        local duration = f2.time - f1.time
        local r,g,b = f2.value[1]*255, f2.value[2]*255, f2.value[3]*255
        return cc.TintTo:create(duration, r,g,b)
    end
    return keyFrameCreate(jsondata, tint)
end

local function keyFrame_position_action_create(jsondata)
    local function moveto(f1, f2)
        local duration = f2.time - f1.time
        local toX, toY = f2.value[1], f2.value[2]
        return cc.MoveTo:create(duration, cc.p(toX, toY))
    end
    return keyFrameCreate(jsondata, moveto)
end

local function keyFrame_rotation_action_create(jsondata)
    local function rotate(f1, f2)
        local duration = f2.time - f1.time
        local to = f2.value
        return cc.RotateTo:create(duration, to)
    end
    return keyFrameCreate(jsondata, rotate)
end

local function keyFrame_opacity_action_create(jsondata)
    local function opacity(f1, f2)
        local duration = f2.time - f1.time
        local to = f2.value * 255
        return cc.FadeTo:create(duration, to)
    end
    return keyFrameCreate(jsondata, opacity)
end

local function keyFrame_scale_action_create(jsondata)
    local function scaleTo(f1, f2)
        local duration = f2.time - f1.time
        local scalex = f2.value[1] 
        local scaley = f2.value[2]
        return cc.ScaleTo:create(duration, scalex, scaley)
    end
    return keyFrameCreate(jsondata, scaleTo)
end

local function keyFrame_skew_action_create(jsondata)
    local function skewTo(f1, f2)
        local duration = f2.time - f1.time
        local skewx = f2.value[1]
        local skewy = f2.value[2]
        return cc.SkewTo:create(duration, skewx, skewy)
    end
    return keyFrameCreate(jsondata, skewTo)
end

local anim_createFuncs = {
    color = keyFrame_color_action_create,
    visible = keyFrame_visible_action_create,
    position = keyFrame_position_action_create,
    rotation = keyFrame_rotation_action_create,
    opacity = keyFrame_opacity_action_create,
    scale = keyFrame_scale_action_create,
    skew = keyFrame_skew_action_create,
}

local function keyFrame_anim_createFunc(animatedProperty)
    local ret = {}
    for actionName, props in pairs(animatedProperty) do
        print("actioname = ", actionName)
        local action = anim_createFuncs[actionName](props)
        if action then
            ret[#ret+1] = action
        end
    end
    return ret
end

function CCBSeq:isNodeExisted(node)
    for i = 1, #self.animatedNodes do
        if node == self.animatedNodes[i] then
            return true
        end
    end
    return false
end

local function fixPositionsData(node, property)
    local data = property["position"]
    if data then
        local frames = data["keyframes"]
        if not node.parent_ then return end
        local siz = node.parent_:getContentSize()
        for _, frame in pairs(frames) do
            -- POSITION_TYPE_PERCENT 2
            frame["value"][1] = frame["value"][1] * (node.posTypeX_ == 2 and siz.width or 1)
            frame["value"][2] = frame["value"][2] * (node.posTypeY_ == 2 and siz.height or 1)
        end
        data["keyframes"] = frames
    end
end

function CCBSeq:addAnimForNode(node, animatedProperty, animName)
    if self:isNodeExisted(node) then
        assert(false, "node should not be added twice!")
        return
    end
    self.animatedNodes[#self.animatedNodes+1] = node
    node.animatedProperties_[animName] = animatedProperty

    -- fix position(位置信息)
    fixPositionsData(node, animatedProperty)
end

-- playSeq 的时候，循环播放要设置回第一帧
local firstFrameSetFunc = {
    CCLayer         = function(node, options) setNodeProps(node, options) end,
    CCSprite        = function(node, options) setNodeProps(node, options) setSpriteProps(node, options) end,
    CCLabelBMFont   = function(node, options) setNodeProps(node, options) end,
    CCScale9Sprite  = function(node, options) setNodeProps(node, options) setScale9SpriteProps(node, options) end,
}

function CCBSeq:setToBase(node)
    firstFrameSetFunc[node.baseClassName](node, node.baseValue)
end

function CCBSeq:getSeqData()
    local callbackChannelAction = keyFrame_callback_createFunc(self.name, self.callbackChannel, self.callbacks)
    local soundCallbackAction = keyFrame_sound_createFunc(self.soundChannel)
    -- 播放actions
    for i = 1, #self.animatedNodes do 
        local node = self.animatedNodes[i]
        local property = node.animatedProperties_[self.name]
        if not property then return end
        -- 这行代码不能加，因为动画应该不和UI坐标相互影响。
        -- self:setToBase(node)
        node:stopAllActions()
        local animatedAction = keyFrame_anim_createFunc(property)
        for j = 1, #animatedAction do
            node:runAction(animatedAction[j])
        end 
    end
    return callbackChannelAction, soundCallbackAction
end

return CCBSeq
