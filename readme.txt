
== Readme ==

1. apptools 目录下的代码基于Cocos2d-x3.7.1
2. tools 目录下的sh文件用于打包或调试流程中，具体看情况更改
3. lua 目录下的代码用于lua层对sprite builder生成的json内容进行解析并操作

todo

1. lua文件可以与cocos2d-x lua再多一点程度的集成，放到到init中，并开启disable_global
2. localization的优化
3. 需要一个全局配置文件
4. 现在可以读取并保存动画信息，所以可以把一些简单的动画分配给其它Node来使用
5. 需要变相支持doc与parent两种方式，参考CreateLayer中的Create1,Create2的使用方式

