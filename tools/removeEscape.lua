
local path = arg[1]

local infile = io.open(path, "r")
local txtfile = infile:read("*a")
infile:close()

local txtfile2 = string.gsub(txtfile, "\\/", "/")
local outfile = io.open(path, "w")
outfile:write(txtfile2)
outfile:close()

print(" SUCCESS on " .. path .. " - " .. os.date())
